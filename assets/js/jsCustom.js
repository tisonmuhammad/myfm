// JQUERY
// MENU SIDEBAR
$(document).ready(function () {
  $('#sidebarCollapse').on('click', function () {
      $('#sidebar').toggleClass('active');
  });
});
$(document).ready(function(){
  if($('.scrollable-container').length > 0){
		$('.scrollable-container').perfectScrollbar();
	}
});

$('.modal').on('shown.bs.modal', function () {
  $('.modal').trigger('focus')
});

// $(document).ready(function(){
//   $(".lang-flag").click(function(){
//     $(".language-dropdown").toggleClass("open");
//   });
//   $("ul.lang-list li").click(function(){
//     $("ul.lang-list li").removeClass("selected");
//     $(this).addClass("selected");
//     if($(this).hasClass('lang-en')){
//       $(".language-dropdown").find(".lang-flag").addClass("lang-en").removeClass("lang-es").removeClass("lang-ina");
//       //$("#lang_selected").html("<p>EN</p>")
//     }else if($(this).hasClass('lang-ina')){
//       $(".language-dropdown").find(".lang-flag").addClass("lang-ina").removeClass("lang-es").removeClass("lang-en");
//       //$("#lang_selected").html("<p>PT</p>")
//     }else{
//       $(".language-dropdown").find(".lang-flag").addClass("lang-es").removeClass("lang-en").removeClass("lang-ina");
//       //$("#lang_selected").html("<p>ES</p>")
//     }
//     $(".language-dropdown").removeClass("open");
//   });
// })

// JAVASCRIPT

$(function(){
  $('.selectpicker').selectpicker();
});

//

// Start upload preview image
// $(".gambar").attr("src", "assets/images/place-user.png");
// Start upload preview image
$(".gambar").attr("src", "assets/images/place-user.png");
  var $uploadCrop,
  tempFilename,
  rawImg,
  imageId;
  function readFile(input) {
    if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
        $('.upload-demo').addClass('ready');
        $('#cropImagePop').modal('show');
              rawImg = e.target.result;
            }
            reader.readAsDataURL(input.files[0]);
        }
        else {
          swal("Sorry - you're browser doesn't support the FileReader API");
      }
  }

  $uploadCrop = $('#upload-demo').croppie({
    viewport: {
      width: 150,
      height: 150,
    },
    enforceBoundary: false,
    enableExif: true
  });
  $('#cropImagePop').on('shown.bs.modal', function(){
    // alert('Shown pop');
    $uploadCrop.croppie('bind', {
          url: rawImg
        }).then(function(){
          console.log('jQuery bind complete');
        });
  });

  $('.item-img').on('change', function () {
    imageId = $(this).data('id'); tempFilename = $(this).val();
  $('#cancelCropBtn').data('id', imageId); readFile(this);
});
  $('#cropImageBtn').on('click', function (ev) {
    $uploadCrop.croppie('result', {
      type: 'base64',
      format: 'jpeg',
      size: {width: 150, height: 150}
    }).then(function (resp) {
      $('#item-img-output').attr('src', resp);
      $('#cropImagePop').modal('hide');
    });
  });
// End upload preview image

$(document).ready(function() {
  $(document).on({
    'show.bs.modal': function() {
      var zIndex = 1040 + (10 * $('.modal:visible').length);
      $(this).css('z-index', zIndex);
      setTimeout(function() {
        $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
      }, 0);
    },
    'hidden.bs.modal': function() {
      if ($('.modal:visible').length > 0) {
        // restore the modal-open class to the body element, so that scrolling works
        // properly after de-stacking a modal.
        setTimeout(function() {
          $(document.body).addClass('modal-open');
        }, 0);
      }
    }
  }, '.modal');
});


// SHOW HIDE LOGIN MY
function show (toBlock){
  setDisplay(toBlock, 'block');
}
function hide (toNone) {
  setDisplay(toNone, 'none');
}
function setDisplay (target, str) {
  document.getElementById(target).style.display = str;
};

// !function(n, e, o) {
//   "use strict";
//   var s = o("html")
//     , l = o("body");
//   o(n).on("load", function() {
//     0 < o(".scrollable-container").length && o(".scrollable-container").perfectScrollbar({
//       theme: "dark"
//     }),
//   })
// }(window, document, jQuery);

